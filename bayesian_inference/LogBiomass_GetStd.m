function [BMD,sigma_biomass_est] = LogBiomass_GetStd(BMD_raw,params)
% This file is a part of the MDSINE program.
%
% MDSINE: Microbial Dynamical Systems INference Engine
% Infers dynamical systems models from microbiome time-series datasets, and
% predicts biologically relevant behaviors of the ecosystems.
% Copyright (C) 2015 Vanni Bucci, Georg Gerber
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% takes log of biomass values, and also returns SD over replicates and
% subjects (that is, the square root of the mean - over all subjects
% and timepoints - of the deviation of each measurement from the mean
% of the _technical replicate_ measurements for that sample; purely an
% indication of measurement error, not biological variability.)
%
% format of BMD_raw is:
% Cell array, one cell per subject, containing 
% a vector of length (num_Replicates * n_timepoints_this_subject)
% in which the time series of biomass estimates from the first 
% technical replicate is followed by the time series of biomass
% estimates from the second technical replicate, and so on.

numReplicates = params.numReplicates;
numSubjects = length(BMD_raw);

BMD = cell(numSubjects,1);

ssd = []; 
for m=1:numSubjects,
    BMD{m} = log(BMD_raw{m});
    n_timepoints_this_subject = length(BMD{m})/numReplicates
    for t=1:n_timepoints_this_subject,
        dt = BMD{m}(t:n_timepoints_this_subject:end)
        dm = mean(dt);
        ssd = [ssd ; (dt-dm).^2];
    end;
end;

sigma_biomass_est = sqrt(mean(ssd));
